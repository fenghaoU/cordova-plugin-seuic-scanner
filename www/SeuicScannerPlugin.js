var execute = require("cordova/exec");

var seuic = {
  nativeListen: function (res, err) {
    return execute(
      res,
      err,
      "SeuicScannerPlugin",
      "requestScan",
      []
    );
  },
  scanSettings: function (res, err, params) {
    return execute(
      res,
      err,
      "SeuicScannerPlugin",
      "setScanParams",
      params
    );
  },
};
module.exports = seuic;

